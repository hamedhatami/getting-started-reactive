package org.acme.getting.started;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class ReactiveGreetingResourceTest {

    static final String PATH_PARAM = "hamed";

    @Test
    public void testHelloEndpoint() {
        given()
                .when().get("/hello/greeting/" + PATH_PARAM)
                .then()
                .statusCode(200)
                .body(is("{\"id\":1,\"birth\":\"2021-03-08T18:09:38.096011Z\",\"name\":\"hamed\",\"status\":\"ACTIVE\"}"));
    }

}
