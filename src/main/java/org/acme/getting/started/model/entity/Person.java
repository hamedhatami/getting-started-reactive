package org.acme.getting.started.model.entity;

import java.time.Instant;

import javax.persistence.Entity;

import io.quarkus.hibernate.reactive.panache.PanacheEntity;

@Entity
public class Person extends PanacheEntity {

    public String name;
    public Instant birth;
    public String status;
}
