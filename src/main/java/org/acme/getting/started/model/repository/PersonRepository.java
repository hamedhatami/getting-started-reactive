package org.acme.getting.started.model.repository;

import org.acme.getting.started.model.entity.Person;

import java.time.Instant;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import io.quarkus.hibernate.reactive.panache.PanacheRepository;
import io.smallrye.mutiny.Uni;

@ApplicationScoped
public class PersonRepository implements PanacheRepository<Person> {

    public Uni<Void> createUser(String name) {
        Person person = new Person();
        person.name = name;
        person.status = "ACTIVE";
        person.birth = Instant.now();
        return person
                .persist()
                .invoke(() -> {
                    if (person.isPersistent()) {
                        System.out.println(">>>>>>>> PERSISTED >>>>>>>>>>>>");
                    }
                })
                .onFailure()
                .recoverWithUni(this::handleConversionFailure);

    }

    private <S> Uni<S> handleConversionFailure(Throwable throwable) {
        return (Uni<S>) Uni.createFrom().item(null);
    }

    public Uni<Person> findByName(String name) {
        return find("name", name).firstResult();
    }

    public Uni<List<Person>> findAlive() {
        return list("status", "ACTIVE");
    }

    public void deleteStefs() {
        delete("name", "Stef");
    }
}
