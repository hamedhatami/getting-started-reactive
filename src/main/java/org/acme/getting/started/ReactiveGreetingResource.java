package org.acme.getting.started;

import org.acme.getting.started.model.entity.Person;
import org.acme.getting.started.model.repository.PersonRepository;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.smallrye.mutiny.Uni;

@Path("/hello")
public class ReactiveGreetingResource {

    @Inject
    PersonRepository personRepository;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/greeting/{name}")
    public Uni<Person> greeting(@PathParam("name") String name) {
         return personRepository.createUser(name)
                .chain(() -> personRepository.findByName(name));
    }

}
